const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/




const demo1 = Object.entries(users);
const demo2 = Object.keys(users);






// Question 1:Find all users who are interested in playing video games.

const answer1 = demo1.filter((entries)=>{
    if(entries[1].interests != undefined){
        let temp_arr = entries[1].interests[0];
        return temp_arr.includes("Video Games");
    }
    return false;

});

console.log("This is answer 1");
console.log(answer1);





// Question 2 : Find all users staying in Germany.

const answer2 = demo1.filter((entries) => {
    return entries[1].nationality == "Germany";

})
console.log("This is answer 2");
console.log(answer2);








// Question 3:  Sort users based on their seniority level for Designation - Senior Developer > Developer > Intern


const seniorityOrder = {
    "Senior": 3,
    "Developer": 2,
    "Intern": 1
};

const answer3_1 = demo2.sort((prev, cur) => {
    const userA = users[prev];
    const userB = users[cur];

    let prev_str = userA.desgination;
    let cur_str = userB.desgination;

    if (prev_str.includes("Senior")) {
        prev_str = "Senior";
    }
    else if (prev_str.includes("Developer")) {
        prev_str = "Developer";
    }
    if (cur_str.includes("Senior")) {
        cur_str = "Senior"
    }
    else if (cur_str.includes("Developer")) {
        cur_str = "Developer";
    }
    if (prev_str.includes("Intern")) {
        prev_str = "Intern";
    }
    if (cur_str.includes("Intern")) {
        cur_str = "Intern"
    }

    const seniorityA = seniorityOrder[prev_str] || 0;
    const seniorityB = seniorityOrder[cur_str] || 0;

    return seniorityB - seniorityA;
});
console.log("This is answer 3.1");
console.log(answer3_1);






// Question 3 : Sort users based on their seniority level for Age - 20 > 10

const answer3_2 = demo2.sort((prev, cur) => {
    const userA = users[prev];
    const userB = users[cur];

    if (userA.age > userB.age) {
        return 1;
    }
    else if (userA.age < userB.age) {
        return -1;
    }
    else {
        return 0;
    }
});
console.log("This is answer 3.2");
console.log(answer3_2);






//Question 4 : Find all users with masters Degree.

const answer4 = demo2.filter((entries) => {
    const user = users[entries];
    let degree_str = user.qualification;
    return degree_str.includes("Master");

});
console.log("This is answer 4");
console.log(answer4);








// Question 5: Group users based on their Programming language mentioned in their designation.

const answer5 = demo1.reduce((acc, entries) => {
    if (acc[entries.desgination]) {
        acc[entries[1].desgination].push(entries[1]);
    }
    else {
        acc[entries[1].desgination] = entries[1];
    }
    return acc;

}, {});

console.log("This is answer 5");
console.log(answer5)




